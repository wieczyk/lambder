PKGNAME=strzykawa
INCLUDES=-I source
OCAMLBUILD_FLAGS=-use-ocamlfind # ${INCLUDES}
OCAMLFIND=ocamlfind
LIB_FILES=
TARGETS=source/lambder.native source/lambder.byte

.PHONY: all clean install remove deinstall uninstall test

all:
	ocamlbuild ${OCAMLBUILD_FLAGS} ${TARGETS}

clean:
	ocamlbuild ${OCAMLBUILD_FLAGS} ${TARGETS} -clean

install: all
	${OCAMLFIND} install ${PKGNAME} META ${LIB_FILES}

remove deinstall uninstall:
	${OCAMLFIND} remove ${PKGNAME}

test: all
	set -e; for f in tests/*.lang; do echo "==> TEST: $$f"; ./lambder.native $$f; done
