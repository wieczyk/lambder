
module Internal = struct

    open Lang_parser
    open Lang_lexer
    open Lang_ast_types

    let open_lexbuf file = 
        let channel = open_in file in
        let lexbuf = Lexing.from_channel channel in
        lexbuf.Lexing.lex_curr_p <- {
            lexbuf.Lexing.lex_curr_p with
            Lexing.pos_fname = file
            };
        lexbuf

    let report_error loc msg = 
        Printf.printf "%s: %s\n%!" (string_of_location loc) msg;
        raise Exit

    let parse lexbuf =
        try
            let token = Lang_lexer.token in
            Lang_parser.file token lexbuf;
        with
        | InvalidToken (loc, str) -> report_error loc "invalid token"
        | Error -> begin
            let loc = Lang_lexer.mkLoc (lexbuf.Lexing.lex_curr_p) in
            report_error loc "syntax error"
        end

    let read f = parse (open_lexbuf f)

end

let parse = Internal.read
