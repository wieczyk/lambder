open Lang_ast

module type ColorTable = sig

  val c_var : string -> string

  val c_fun : string -> string

  val c_ref : string -> string

  val c_muid : string -> string

  val c_mu : string -> string

  val c_toplevel: string -> string

  val c_syntax: string -> string 

  val c_const: string -> string 
end

module NoColors : ColorTable = struct

  let id x = x
  let c_var = id

  let c_fun = id

  let c_ref = id

  let c_muid = id

  let c_mu = id

  let c_toplevel = id

  let c_syntax = id

  let c_const = id

end

module TtyColors : ColorTable = struct

  let esc_attrs attrs x = 
    let final = String.concat ";" (List.map string_of_int attrs) in
    Printf.sprintf "\o033[%sm%s\o033[0m" final x

  let c_var = esc_attrs [33]

  let c_fun = esc_attrs [34; 1] 

  let c_ref = esc_attrs [32]

  let c_muid = esc_attrs [36]

  let c_mu = esc_attrs [35; 1]

  let c_toplevel = esc_attrs [31; 1]

  let c_syntax = esc_attrs []

  let c_const = esc_attrs[37;1]

end

module type KeywordTable = sig 
  val kw_fun : string

  val kw_mu : string 
end

module AsciiKeywordTable: KeywordTable = struct
  let kw_fun = "fun"

  let kw_mu = "mu"
end

module UTF8KeywordTable : KeywordTable = struct 
  let kw_fun = "λ"

  let kw_mu = "μ"
end

module Implementation (Colors: ColorTable)(Kw: KeywordTable) = struct
  open Colors
  open Kw

  let string_of_toplevel_identifier (TopLevel_Identifier id) = c_ref ("$" ^ id)

  let string_of_identifier (Identifier id) = c_var id

  let string_of_mu_identifier (Mu_Identifier id) = c_muid ("'" ^ id)

  let string_of_constant (Constant id) = c_const ("^" ^ id)

  let app_requires_parenthesis_lhs = function 
  | Var _
  | Ref _
  | Const _
  | App _ -> false
  | _ -> true

  let app_requires_parenthesis_rhs = function 
  | Var _
  | Const _
  | Ref _ -> false
  | App _ 
  | _ -> true

  let rec string_of_expression = function
  | Ref id  ->
    string_of_toplevel_identifier id
  | Const id ->
    string_of_constant id
  | Var id ->
    string_of_identifier id
  | Fun (id, body) ->
     let identifiers, body = collect_bound_vars [id] body in
     String.concat ""
      [ c_fun kw_fun
      ; " "
      ; String.concat " " (List.map string_of_identifier identifiers)
      ; c_syntax "."
      ; " "
      ; string_of_expression body
      ]
    | App (f, x) -> String.concat ""
      [ if app_requires_parenthesis_lhs f then c_syntax "(" else ""
      ; string_of_expression f
      ; if app_requires_parenthesis_lhs f then c_syntax ")" else ""
      ; " "
      ; if app_requires_parenthesis_rhs x then c_syntax "(" else ""
      ; string_of_expression x
      ; if app_requires_parenthesis_rhs x then c_syntax ")" else ""
      ]
    | Mu (a, (b, e)) -> String.concat ""
      [ c_mu kw_mu
      ; " "
      ; string_of_mu_identifier a
      ; c_syntax "."
      ; " "
      ; c_syntax "["
      ; string_of_mu_identifier b
      ; c_syntax "]"
      ; " "
      ; if app_requires_parenthesis_rhs e then c_syntax "(" else ""
      ; string_of_expression e
      ; if app_requires_parenthesis_rhs e then c_syntax ")" else ""
      ]
  and collect_bound_vars xs = function 
    | Fun (id, body) -> collect_bound_vars (id::xs) body 
    | body -> (List.rev xs, body)

  let string_of_toplevel_expression = function
    | TopLevel_Expression e -> string_of_expression e
    | TopLevel_Apply (fs, e) -> String.concat ""
      [ c_toplevel "Apply"
      ; " ["
      ; String.concat " " fs
      ; "] "
      ; string_of_expression e
      ]

  let string_of_module_item = function
  | Print e -> String.concat " "
    [ c_toplevel "Print"
    ; string_of_toplevel_expression e
    ]
  | Macro (id, e) -> String.concat " " 
    [ c_toplevel "Macro"
    ; string_of_toplevel_identifier id
    ; c_syntax "="
    ; string_of_toplevel_expression e
    ]
  | Assert (e1, e2, exp) -> String.concat " " 
    [ c_toplevel "Assert"
    ; string_of_toplevel_expression e1
    ; c_syntax (if exp then "=" else "!=")
    ; string_of_toplevel_expression e2
    ]

end

module IMPL = Implementation(TtyColors)(UTF8KeywordTable)
include IMPL

let __print f x = print_endline (f x)

let print_module_item = __print string_of_module_item 
