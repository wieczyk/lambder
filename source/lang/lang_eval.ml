open Lang_ast

type step
  = FreshVar of identifier * identifier 
  | FreshMuVar of mu_identifier * mu_identifier 
  | LambdaBetaRed of identifier
  | MuRed 
  | MuEtaRed
  | MuBetaRed
  | Unfold of toplevel_identifier 
  | Fold of toplevel_identifier 
  | NoRedex
  | NothingToDo
  | UnknownMacro of toplevel_identifier

module SyntaxUtils = struct

  let rec substitute_ env fv mu_fv cont_success cont_failure x repl expr = match expr with 
    | Var y when x = y ->
      cont_success repl

    | Fun (y, _) when x = y ->
      cont_success expr 

    | Mu (a, n) -> 
      if mu_is_free env repl a then 
        let a' = mu_fresh mu_fv in 
        cont_failure (Mu (a', mu_rename_named a a' n)) (FreshMuVar (a, a'))
      else
        let (b, e) = n in 
        let subcont_success e' = cont_success (Mu (a, (b, e'))) in
        let subcont_failure e' = cont_failure (Mu (a, (b, e'))) in
        substitute_ env fv mu_fv subcont_success subcont_failure x repl e

    | App (f, a) -> 
        let subcont_f_success f' = 
          let subcont_a_success a' = cont_success (App (f', a')) in 
          let subcont_a_failure a' = cont_failure (App (f, a')) in 
          substitute_ env fv mu_fv subcont_a_success subcont_a_failure x repl a
        in
        let subcont_f_failure f' = cont_failure (App (f', a)) in
        substitute_ env fv mu_fv subcont_f_success subcont_f_failure x repl f

    | Fun (y, body) ->
        if is_free env repl y then
          let y' = fresh fv in 
          cont_failure (Fun (y', rename y y' body)) (FreshVar (y, y'))
        else 
          let subcont_success body' = cont_success (Fun (y, body')) in
          let subcont_failure body' = cont_failure (Fun (y, body')) in 
          substitute_ env fv mu_fv subcont_success subcont_failure x repl body

    | Ref id ->
      if Hashtbl.mem env id then
          if (is_free env expr x) then
            cont_failure (Hashtbl.find env id) (Unfold id)
          else
            cont_success expr

      else
          cont_failure expr (UnknownMacro id)

    | Const _ 
    | Var _  ->
      cont_success expr

  let substitute env cont_success cont_fresh expr x repl = 
    let fv = fv env [expr; repl] in
    let mu_fv = mu_fv env [expr; repl] in
    if not (Hashtbl.mem fv x) then
      cont_success expr 
    else 
      substitute_ env fv mu_fv cont_success cont_fresh x repl expr

  let rec mu_substitute_ env fv mu_fv cont_success cont_failure x' repl expr = match expr with
    | Const _ 
    | Var _ ->
      cont_success expr

    | Mu (a, _) when x' = a ->
      cont_success expr

    | Fun (y, body) -> 
      if is_free env (snd repl) y then 
        let y' = fresh fv in
        cont_failure (Fun (y', rename y y' body)) (FreshVar (y, y'))
      else
        let subcont_success body' = cont_success (Fun (y, body')) in
        let subcont_failure body' = cont_failure (Fun (y, body')) in
        mu_substitute_ env fv mu_fv subcont_success subcont_failure x' repl body

    | App (f, a) -> 
      let subcont_f_success f' = 
        let subcont_a_success a' = cont_success (App (f', a')) in 
        let subcont_a_failure a' = cont_failure (App (f, a')) in 
        mu_substitute_ env fv mu_fv subcont_a_success subcont_a_failure x' repl a
      in
      let subcont_f_failure f' = cont_failure (App (f', a)) in
      mu_substitute_ env fv mu_fv subcont_f_success subcont_f_failure x' repl f

    | Mu (a, n) ->
      if mu_is_free env (snd repl) x' then
        let a' = mu_fresh mu_fv in 
        cont_failure (Mu (a', mu_rename_named a a' n)) (FreshMuVar (a, a'))
      else
        let subcont_success n' = cont_success (Mu (a, n')) in
        let subcont_failure n' = cont_failure (Mu (a, n')) in
        mu_substitute_named_ env fv mu_fv subcont_success subcont_failure x' repl n
          
    | Ref id ->
      if Hashtbl.mem env id then
          if (mu_is_free env expr x') then
            cont_failure (Hashtbl.find env id) (Unfold id)
          else
            cont_success expr
      else
          cont_failure expr (UnknownMacro id)

  and mu_substitute_named_ env fv mu_fv cont_success cont_failure x repl (b, e) =
      let subcont_success e' = 
          if x = b then begin
            Printf.printf "Success\n%!";
            cont_success (b, (fst repl) e' (snd repl))
          end else
            cont_success (b, e') 
          in
      let subcont_failure e' = cont_failure (b, e') in
      Printf.printf "[subst in %s  for var b=%s; x=%s]\n%!" (Lang_printer.string_of_expression e) (Lang_printer.string_of_mu_identifier b) (Lang_printer.string_of_mu_identifier x);
      mu_substitute_ env fv mu_fv subcont_success subcont_failure x repl e

  let mu_substitute_named env cont_success cont_failure topexpr expr x repl = 
    let fv = fv env [topexpr] in 
    let mu_fv = mu_fv env [topexpr] in 
    mu_substitute_named_ env fv mu_fv cont_success cont_failure x repl expr
end

module CallByName = struct
  open SyntaxUtils

  let rec reduction cont env whnf_only e = match e with
    | Const _ 
    | Var _ ->
      cont e NoRedex 

    | Ref id -> 
      if Hashtbl.mem env id then 
        cont (Hashtbl.find env id) (Unfold id)
      else 
        cont e (UnknownMacro id)

    | App (Fun (x, body), a) ->
      let cont_success e = cont e (LambdaBetaRed x) in
      let cont_failure e = cont (App(Fun (x, e), a)) in
      substitute env cont_success cont_failure body x a

    | App (Mu (a, n), m) ->
      let cont_success n' = cont (Mu (a, n')) (MuRed) in
      let cont_failure n' r = cont (App (Mu (a, n'), m)) r in 
      mu_substitute_named env cont_success cont_failure e n a ((fun e' m' -> App(e', m')), m)

    | App (f, a) ->
      let cont_after_f f' result =begin  match result with
        | NoRedex when not whnf_only -> 
          let cont_after_a a' = cont (App (f', a')) in
          reduction cont_after_a env whnf_only a
        | _ ->
          cont (App (f', a)) result
      end in
      reduction cont_after_f env whnf_only f

    | Mu (a, (b, e)) when a = b && not (mu_is_free env e b) ->
      cont e MuEtaRed

    | Mu (a, (b, Mu (c, (d, e))) ) ->
      cont (Mu (a, (d, mu_rename c b e))) MuBetaRed

    | Mu (a, (b, n)) ->
      if whnf_only then
        cont e NoRedex
      else
        let cont_after_e e' result = cont (Mu (a, (b, e'))) result in
        reduction cont_after_e env whnf_only n

    | Fun (x, body) ->
      if whnf_only then
        cont e NoRedex
      else
        let cont_after_body body' result = cont (Fun (x, body')) result in
        reduction cont_after_body env whnf_only body

  let cps env e = 
    let fv = fv env [e] in
    let mapping = Hashtbl.create 513 in 
    let mapa a = 
      match Hashtbl.find_opt mapping a with 
      | Some aa -> aa
      | None -> fresh fv
      in
    let rec trans = function 
        Const c ->
          let k = fresh fv in
          Fun (k, App(Var k, Const c))
      | Var x as e ->
        e
      | Ref id -> 
          trans (Hashtbl.find env id)
      | App (f, a) -> 
          let k = fresh fv in 
          let m = fresh fv in 
          Fun (k, App (trans f, Fun (m, App(App(Var m, trans a), Var k))))
      | Fun (x, m) ->
          let k = fresh fv in 
          Fun (k, App (Var k, Fun (x, trans m)))
      | Mu (a, n) -> 
          let aa = fresh fv in
          let v = fresh fv in
          Hashtbl.replace mapping a aa;
          Fun (aa, App (trans_named n, Fun(v, Var v) ))
    and trans_named (a, m) = 
      let d = fresh fv in 
      let aa = mapa a in
      Fun (d, App(trans m, Var aa))
    in
    trans e
  end

module CallByValue = struct
  open SyntaxUtils

  let rec reduction cont env enf_only e = match e with
    | Const _ 
    | Var _ ->
      cont e NoRedex 

    | Ref id -> 
      if Hashtbl.mem env id then 
        cont (Hashtbl.find env id) (Unfold id)
      else 
        cont e (UnknownMacro id)

    | App (f, a) ->
      let cont_after_f f' result = match result with
        | NoRedex -> 
          let cont_after_a a' result =
            match result with
            | NoRedex ->
              begin match f' with
              | Fun (x, body) ->
                let cont_success e = cont e (LambdaBetaRed x) in
                let cont_failure e = cont (App(Fun (x, e), a')) in
                substitute env cont_success cont_failure body x a'

              | Mu (a, n) ->
                let cont_success n' = cont (Mu (a, n')) (MuRed) in
                let cont_failure n' r = cont (App (Mu (a, n'), a')) r in 
                Printf.printf "#1\n%!";
                mu_substitute_named env cont_success cont_failure e n a ((fun e' m' -> App(e', m')) , a')

              | _ ->
                begin match a' with
                | Mu (a, n) ->
                  let cont_success n' = cont (Mu (a, n')) (MuRed) in
                  let cont_failure n' r = cont (App (f', Mu (a, n'))) r in 
                  mu_substitute_named env cont_success cont_failure e n a ((fun e' m' -> App(m', e')) , f')
                | _ ->
                  cont (App (f', a')) result 
                end
              end
            | _ ->
              cont (App (f', a')) result 
           in
          reduction cont_after_a env enf_only a
        | _ ->
          cont (App (f', a)) result
      in
      reduction cont_after_f env enf_only f

    | Mu (a, (b, e)) when a = b && not (mu_is_free env e b) ->
      cont e MuEtaRed

    | Mu (a, (b, Mu (c, (d, e))) ) ->
      cont (Mu (a, (d, mu_rename c b e))) MuBetaRed

    | Mu (a, (b, n)) ->
      if enf_only then
        cont e NoRedex
      else
        let cont_after_e e' result = cont (Mu (a, (b, e'))) result in
        reduction cont_after_e env enf_only n

    | Fun (x, body) ->
      if enf_only then
        cont e NoRedex
      else 
        let cont_after_body body' result = cont (Fun (x, body')) result in
        reduction cont_after_body env enf_only body
end

module MacroFolder = struct 
  let macro_folder_instant env = function
    | Ref _ -> None
    | e -> 
      let exception Success of toplevel_identifier in 
      try 
        let f k v = if alphaeq_with_env env v e then raise (Success k) in 
        Hashtbl.iter f env;
        None
      with Success(id) -> 
        Some id

  let rec macro_folder cont env e =
    match macro_folder_instant env e with 
    | Some id -> cont (Ref id) (Fold id)
    | None -> begin match e with
      | Fun (x, b) -> 
        let subcont b' = cont (Fun (x, b')) in
        macro_folder subcont env b

      | App (f, a) ->
        let subcont_f f' = function
          | NothingToDo -> 
            let subcont_a a' = cont (App (f', a')) in
            macro_folder subcont_a env a
          | reson -> cont (App (f', a)) reson 
        in
        macro_folder subcont_f env f

      | Mu (a, (b, e)) -> 
        let subcont_e e' = cont (Mu (a, (b, e')))  in
        macro_folder subcont_e env e

      | _ ->
        cont e NothingToDo

      end
end
