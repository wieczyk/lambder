%{
(*
 * Pawel Wieczorek
 *)

open Lang_ast_types
open Lang_ast

let mkLoc pos = 
    (pos.Lexing.pos_fname, pos.Lexing.pos_lnum, pos.Lexing.pos_cnum - pos.Lexing.pos_bol + 1)

let build_abstraction wrap idents body =
    List.fold_right wrap idents body

let build_fun idents body =
    build_abstraction (fun id e -> Fun (id, e)) idents body

%}


%token FUN, MU, MACRO, PRINT,  ASSERT, EVAL

%token SQUARE_OPEN, SQUARE_CLOSE
%token BRACKET_OPEN, BRACKET_CLOSE
%token DOT

%token <string> IDENTIFIER
%token <string> APOSTROF_IDENTIFIER
%token <string> DOLAR_IDENTIFIER
%token <string> CARET_IDENTIFIER
%token EQ, EQEQ, NE
%type <Lang_ast.module_item list> file
%token EOF

%start file
%%

/* ------------------------------ */


file:
    | many(module_item) EOF
    { $1 }

functions:
    | SQUARE_OPEN many(IDENTIFIER) SQUARE_CLOSE
    { $2 }

toplevel_expression:
    | EVAL functions expression
    { TopLevel_Apply ($2, $3) }
    | functions expression
    { TopLevel_Apply ($1, $2) }
    | expression
    { TopLevel_Expression $1 }

module_item:
    | MACRO toplevel_identifier EQ toplevel_expression
    { Macro ($2, $4) }
    | PRINT toplevel_expression
    { Print $2 }
    | ASSERT toplevel_expression eq toplevel_expression
    { Assert($2, $4, $3) }

eq:
    | EQEQ { true }
    | NE { false }

/* ------------------------------ */

expression:
    | FUN many1(ident) DOT expression
    { build_fun $2 $4 }
    | MU mu_identifier DOT named_expression
    { Mu ($2, $4) }
    | expression_applicative
    { $1 }

named_expression:
    | SQUARE_OPEN mu_identifier SQUARE_CLOSE expression_atom
    { ($2, $4) }

expression_applicative:
    | expression_applicative expression_atom
    { App ($1, $2) }
    | expression_atom
    { $1 }

expression_atom:
    | BRACKET_OPEN expression BRACKET_CLOSE
    { $2  }
    | ident
    { Var $1 }
    | toplevel_identifier
    { Ref $1 }
    | constant
    { Const $1 }

ident:
    | IDENTIFIER
    { Identifier $1 }

toplevel_identifier:
    | DOLAR_IDENTIFIER
    { TopLevel_Identifier $1 }

mu_identifier:
    | APOSTROF_IDENTIFIER
    { Mu_Identifier $1 }

constant:
    | CARET_IDENTIFIER
    { Constant $1 }

/* ------------------------------ */

opt(P):
    | { None }
    | P { Some $1 }

raw_many(P):
    | 
    { [] }

    | raw_many(P) P
    { $2::$1 }

many(P):
    | raw_many(P)
    { List.rev $1 }

many1(P):
    | P many(P)
    { $1 :: $2 }

prefix(S,P):
    | S P
    { $2 }

suffix(P,S):
    | P S
    { $1 }

manySep(S,P):
    | 
    { [] }

    | manySep1(S, P)
    { $1 }

manySep1(S,P):
    | P many(prefix(S,P))
    { $1 :: $2 }

manySep2(S,P):
    | P S P many(prefix(S,P))
    { $1 :: $3 :: $4 }

binop_tier(O, N):
    | N
    { () }
    | binop_tier(O, N) O N
    { () }
    ;

unop_tier(O, N):
    | N
    { () }
    | O unop_tier(O, N)
    { () }
