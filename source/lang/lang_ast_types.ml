type identifier = Identifier of string

type location = Location of
  { file : string
  ; line: int
  ; column: int
  }

type span_location = location * location

let string_of_location (Location l) =
    Printf.sprintf "%s:%u:%u" l.file l.line l.column

let string_of_identifier (Identifier n) = n
