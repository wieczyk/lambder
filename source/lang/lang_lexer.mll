(* Pawel Wieczorek
 *)

{
open Lang_parser
open Lang_ast_types
open Lexing

exception InvalidToken of Lang_ast_types.location * string

let keywords =
    [ "fun", FUN
    ; "mu", MU
    ; "Macro", MACRO
    ; "Print", PRINT
    ; "Assert", ASSERT
    ; "Eval", EVAL
    ]

let symbols =
    [ "(", BRACKET_OPEN
    ; ")", BRACKET_CLOSE
    ; "[", SQUARE_OPEN
    ; "]", SQUARE_CLOSE
    ; ".", DOT
    ; "=", EQ
    ; "==", EQEQ
    ; "!=", NE
    ]

let makeTable mapping =
    let h = Hashtbl.create 127 in
    List.iter (fun (str, key) -> Hashtbl.replace h str key) mapping;
    h

let keywordsTable = makeTable keywords
let symbolTable = makeTable symbols

let mkString str =
    try
        Hashtbl.find keywordsTable str
    with Not_found ->
        IDENTIFIER str

let lookupSymbol str =
    try
        Hashtbl.find symbolTable str
    with Not_found ->
        failwith "unknown token symbol"

let mkLoc pos = 
    Location 
        { file = pos.Lexing.pos_fname
        ; line = pos.Lexing.pos_lnum
        ; column =  pos.Lexing.pos_cnum - pos.Lexing.pos_bol + 1
        }

let handleError pos token =
    let exc = InvalidToken (mkLoc pos, token) in
    raise exc


let update_loc lexbuf = 
    let pos = lexbuf.lex_curr_p  in
    lexbuf.lex_curr_p <- { pos with
      pos_lnum  = pos.pos_lnum + 1;
      pos_bol   = pos.pos_cnum
    }
}

let digit   = ['0'-'9']
let id        = ['a'-'z' '_' 'A' - 'Z']['_' 'A' - 'Z' 'a'-'z' '0'-'9']*
let symbols = ( "." | "[" | "]" | "(" | ")" | "=" | "!=" | "==")

rule token = parse
    | ['\n']
    { update_loc lexbuf; token lexbuf }

    | "//" [^'\n']* ['\n']
    { update_loc lexbuf; token lexbuf }

    | "//" [^'\n']* eof
    { EOF }

    | [' ' '\t' '\r']
    { token lexbuf }

    | '$' (id as id)
    { DOLAR_IDENTIFIER id }

    | "'" (id as id)
    { APOSTROF_IDENTIFIER id }

    | "^" (id as id)
    { CARET_IDENTIFIER id }

    | id as id
    { mkString id }

    | symbols as symbol
    { lookupSymbol symbol }

    | eof
    { EOF }

    | _
    { handleError (Lexing.lexeme_start_p lexbuf) (Lexing.lexeme lexbuf) }

{

}
