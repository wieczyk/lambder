open Lang_ast_types

type toplevel_identifier = TopLevel_Identifier of string
type mu_identifier = Mu_Identifier of string
type identifier = Identifier of string
type constant = Constant of string 

type expression
    = Fun of identifier * expression
    | App of expression * expression
    | Var of identifier
    | Ref of toplevel_identifier
    | Mu of mu_identifier * named_expression
    | Const of constant
and named_expression
    = mu_identifier * expression

type toplevel_expression
    = TopLevel_Expression of expression
    | TopLevel_Apply of string list * expression

type module_item
    = Macro of toplevel_identifier * toplevel_expression
    | Print of toplevel_expression
    | Assert of toplevel_expression * toplevel_expression * bool

let fv env es =
  let data = Hashtbl.create 513 in
  let rec preserve x f =
    let has_x = Hashtbl.mem data x in 
    f();
    if has_x then Hashtbl.replace data x () else Hashtbl.remove data x
  in

  let rec aux = function
    | Var x -> Hashtbl.replace data x ()
    | Const _ -> ()
    | App (f, a) -> aux f; aux a
    | Ref id -> 
      if Hashtbl.mem env id then
        aux (Hashtbl.find env id)
    | Mu (_, (_, e)) -> aux e
    | Fun (x, body) ->
      preserve x (fun () -> aux body);
      ()
  in List.iter aux es; data

let mu_fv env es =
  let data = Hashtbl.create 513 in
  let rec preserve x f =
    let has_x = Hashtbl.mem data x in 
    f();
    if has_x then Hashtbl.replace data x () else Hashtbl.remove data x
  in

  let rec aux = function
    | Var x ->  ()
    | Const _ -> ()
    | App (f, a) -> aux f; aux a
    | Ref id -> 
      if Hashtbl.mem env id then
        aux (Hashtbl.find env id)
    | Mu (a, (b, e)) -> 
        preserve a (fun () ->
          Hashtbl.add data b ();
          aux e
        )
    | Fun (x, body) ->
      aux body
  in List.iter aux es; data


let is_free env e x = 
    Hashtbl.mem (fv env [e]) x

let mu_is_free env e x =
    Hashtbl.mem (mu_fv env [e]) x

let fresh fv = 
  let rec loop i = 
    let id = Identifier ("o" ^ string_of_int i) in
    if Hashtbl.mem fv id then loop (succ i) else begin
        Hashtbl.add fv id ();
        id
    end
  in loop 0

let mu_fresh fv = 
  let rec loop i = 
    let id = Mu_Identifier ("i" ^ string_of_int i) in
    if Hashtbl.mem fv id then loop (succ i) else begin
        Hashtbl.add fv id ();
        id
    end
  in loop 0

let rec rename x y expr = match expr with 
    | Var z when z = x -> Var y 
    | Var _ | Const _ | Ref _ -> expr
    | App (f, a) -> App (rename x y f, rename x y a)
    | Mu (a, (b, e)) -> Mu (a, (b, rename x y e))
    | Fun (z, _) when z = x -> expr
    | Fun (z, body) -> Fun (z, rename x y body)

let rec mu_rename x y expr = match expr with 
    | Var _ | Const _ | Ref _ -> expr
    | App (f, a) -> App (mu_rename x y f, mu_rename x y a)
    | Mu (a, n) when a = x -> Mu (a, n)
    | Mu (a, n) -> Mu (a, mu_rename_named x y n)
    | Fun (z, body) -> Fun (z, mu_rename x y body)

and mu_rename_named x y (b, e) = 
    let c = if x = b then y else b in 
    (c, mu_rename x y e)

let alphaeq_with_env env expr expr' =
  let data = Hashtbl.create 513 in
  let mu_data = Hashtbl.create 513 in
  let set_eq x x' = 
      Hashtbl.replace data x x'; 
      Hashtbl.replace data x' x
  in
  let mu_set_eq x x' = 
      Hashtbl.replace mu_data x x' ;
      Hashtbl.replace mu_data x' x
  in
  let check_eq x x' = 
      let kw = Hashtbl.find_opt data x in
      kw = Some x' || (kw = None && x = x')
      in
  let mu_check_eq x x' = 
      let kw = Hashtbl.find_opt mu_data x in
      kw = Some x' || (kw = None && x = x')
      in
  let assume_eq x x' f = 
      let oldeq = Hashtbl.find_opt data x in
      set_eq x x';
      let r = f () in 
      begin match oldeq with
      | Some x' -> set_eq x x' 
      | None -> Hashtbl.remove data x; Hashtbl.remove data x'
      end;
      r
      in
  let mu_assume_eq x x' f = 
      let oldeq = Hashtbl.find_opt mu_data x in
      mu_set_eq x x';
      let r = f () in 
      begin match oldeq with
      | Some x' -> mu_set_eq x x' 
      | None -> Hashtbl.remove mu_data x
      end;
      r
    
  in
  let rec aux = function
    | (Var x, Var x') ->
        check_eq x x'
    | (Ref id, Ref id') ->
        id = id' || 
          ( Hashtbl.mem env id 
          && Hashtbl.mem env id'
          && aux (Hashtbl.find env id, Hashtbl.find env id')
        )
    | (App (f, a), App (f', a')) -> 
        aux (f, f') && aux (a, a')
    | (Mu (a, (b, e)), Mu (a', (b', e'))) -> 
        mu_assume_eq a a' (fun () ->
          mu_check_eq b b' && aux (e, e')
        )
    | (Fun (x, b), Fun (x', b')) -> 
        assume_eq x x' (fun () -> aux (b, b'))
    | (Ref id, e') ->
        not (Hashtbl.mem env id) || (aux (Hashtbl.find env id, e'))
    | (e', Ref id) ->
        not (Hashtbl.mem env id) || (aux (Hashtbl.find env id, e'))
    | Const c, Const c' -> 
        c = c'
    | _, _ ->
        false
  in
  aux (expr, expr')
