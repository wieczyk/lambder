open Lang_ast
open Lang_printer
open Lang_eval

module Implementation () = struct

  type environment = (toplevel_identifier, expression) Hashtbl.t

  let env: environment = Hashtbl.create 513

  let rec evalloop red under_binder e =
    Printf.printf "\t%s\n%!" (string_of_expression e);
    red (evalcont red under_binder) env under_binder  e

  and evalcont red under_binder e = function
    | NoRedex -> 
      Printf.printf "\n\tCannot find redex.\n%!";
      e
    | NothingToDo -> 
      e
    | UnknownMacro id -> 
      Printf.printf "\n\tUnknown macro %s.\n%!" 
        (string_of_toplevel_identifier id);
      e
      
    | Unfold id ->
      let expr = Hashtbl.find env id in
      Printf.printf "\n\tUnfolding macro %s as %s:\n%!"
        (string_of_toplevel_identifier id)
        (string_of_expression expr);
      evalloop red under_binder e
    | Fold id ->
      let expr = Hashtbl.find env id in
      Printf.printf "\n\tFolding macro %s (defined as %s):\n%!"
        (string_of_toplevel_identifier id)
        (string_of_expression expr);
      evalloop red under_binder e
    | FreshVar (oldv, newv) ->
      Printf.printf "\n\tRenamnig %s -> %s:\n%!"
        (string_of_identifier oldv)
        (string_of_identifier newv);
      evalloop red under_binder e
    | FreshMuVar (oldv, newv) ->
      Printf.printf "\n\tRenamnig %s -> %s:\n%!"
        (string_of_mu_identifier oldv)
        (string_of_mu_identifier newv);
      evalloop red under_binder e
    | LambdaBetaRed id ->
      Printf.printf "\n\tLambda beta-reduction (variable %s):\n%!" 
        (string_of_identifier id);
      evalloop red under_binder e
    | MuBetaRed ->
      Printf.printf "\n\tMu beta-reduction:\n%!";
      evalloop red under_binder e
    | MuRed ->
      Printf.printf "\n\tMu reduction:\n%!";
      evalloop red under_binder e
    | MuEtaRed ->
      Printf.printf "\n\tMu eta-reduction:\n%!";
      evalloop red under_binder e

  let can_fold_macros e = 
      let cont e = function
        | NothingToDo -> false 
        | UnknownMacro _ -> false
        | _ -> true
      in
      MacroFolder.macro_folder cont env e

  let eval_cps e = 
      Printf.printf "\n\tTranslation to CPS:\n%!";
      let e = CallByName.cps env e in
      Printf.printf "\t%s\n%!" (string_of_expression e);
      e

  let eval_direct e =
      Printf.printf "\n\tStart:\n%!";
      evalloop CallByValue.reduction e 

  let eval_function = function
    | "CpsN" ->  fun e -> 
      Printf.printf "\n\tTranslation to Continuation-Passing-Style for applicative strategy\n%!";
      let e = CallByName.cps env e in
      Printf.printf "\t%s\n%!" (string_of_expression e);
      e

    | "NfN" -> fun e -> 
      Printf.printf "\n\tFull normalization with normal strategy\n%!";
      evalloop CallByName.reduction false e

    | "NfV" -> fun e -> 
      Printf.printf "\n\tFull normalization with applicative strategy\n%!";
      evalloop CallByValue.reduction false e

    | "Whnf" -> fun e -> 
      Printf.printf "\n\tWeak normalization with normal strategy\n%!";
      evalloop CallByName.reduction true e

    | "Enf" -> fun e -> 
      Printf.printf "\n\tWeak normalization with applicative strategy\n%!";
      evalloop CallByValue.reduction true e

    | f -> fun e ->
      Printf.printf "\n\tUnknown function: %s\n%!" f;
      e

  let rec eval_toplevel_functions e = function
    | [] -> 
      Printf.printf "\n\tFinal result:\n\t%s\n%!" (string_of_expression e);
      e
    | f::fs ->
      let e = eval_function f e in
      eval_toplevel_functions e fs

  let eval_toplevel_expression = function
    | TopLevel_Expression e -> eval_toplevel_functions e []
    | TopLevel_Apply (fs, e) -> eval_toplevel_functions e (List.rev fs)

  let interpret_ = function 
    | Print e ->
      ignore (eval_toplevel_expression e)
    | Macro (id, e) ->
      let e = eval_toplevel_expression e in
      Hashtbl.replace env id e;
      Printf.printf "\n\tDefined %s.\n%!" (string_of_toplevel_identifier id)
    | Assert(e, e', exp) ->
      Printf.printf "\n\tLHS\n%!";
      let e = eval_toplevel_expression e in
      Printf.printf "\n\tRHS\n%!";
      let e' = eval_toplevel_expression e' in 
      let result = alphaeq_with_env env e e' in
      Printf.printf "\n\tTerms are %s.\n%!" (if result then "equal" else "not equal");
      if result != exp then failwith "Assertion failed"

  let interpret mi =
    Printf.printf "%s\n%!" (string_of_module_item mi);
    interpret_ mi;
    Printf.printf "\n%!"

  let interpret_sequence = List.iter interpret
end

let interpret_sequence mis = 
    let module M = Implementation () in 
    M.interpret_sequence mis