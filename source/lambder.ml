open File_reader
open Lang_toplevel
let run f = f (); 0

let _ = exit begin
    run begin fun () ->
        let fname = Array.get Sys.argv 1 in
        Printf.printf "Compiling %s\n%!" fname;
        let items = File_reader.parse fname in
        interpret_sequence items
    end end

